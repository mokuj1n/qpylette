import colorsys


def sort_by(hcolor):
    h = hcolor.replace('#', '')
    r, g, b = tuple(int(h[i:i+2], 16) for i in (0, 2, 4))
    return colorsys.rgb_to_hls(r, g, b)


def rgb_to_hex(color):
    """Convert an rgb color to hex."""
    return "#%02x%02x%02x" % (*color,)


def get_colors(raw_colors, sort=False):
    colors = [rgb_to_hex([*color[0]]) for color in raw_colors]
    if sort:
        colors.sort(key=sort_by)
    return colors
