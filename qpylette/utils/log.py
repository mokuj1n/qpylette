import logging

logging.basicConfig(level=logging.ERROR)

app = logging.getLogger('app')
init = logging.getLogger('init')
workers = logging.getLogger('workers')
imgview = logging.getLogger('imgview')
mainwindow = logging.getLogger('mainwindow')
