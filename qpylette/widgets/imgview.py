import os

from PySide6 import QtCore, QtGui
from PySide6.QtCore import Signal
from PySide6.QtWidgets import (QFileDialog, QFrame, QGraphicsScene,
                               QGraphicsView, QMessageBox)
from qpylette.utils import log


class ImageViewScene(QGraphicsScene):

    drop_file_signal = Signal(str)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        files = [u.toLocalFile() for u in event.mimeData().urls()]
        if len(files) > 1:
            QMessageBox.critical(None, "Drag and drop error",
                                 "Only one file can be opened!",
                                 QMessageBox.StandardButton.Close)
            return

        elif (0 < len(files) < 2):
            file = files[0]
            if os.path.isdir(file):
                QMessageBox.critical(None, "File open error",
                                     f"{file} is not file!",
                                     QMessageBox.StandardButton.Close)
                return
            self.drop_file_signal.emit(file)

    def dragMoveEvent(self, event):
        event.acceptProposedAction()


class ImageView(QGraphicsView):

    STYLESHEET = """
    QGraphicsView {
        border: none;
        background-color: rgba(0, 0, 0, 0);
    }
    """

    leftMouseButtonPressed = Signal(float, float)
    rightMouseButtonPressed = Signal(float, float)
    leftMouseButtonReleased = Signal(float, float)
    rightMouseButtonReleased = Signal(float, float)
    leftMouseButtonDoubleClicked = Signal(float, float)
    rightMouseButtonDoubleClicked = Signal(float, float)

    def __init__(self, parent) -> None:
        super().__init__(parent=parent)
        self.mainwindow = parent

        self.setFrameShape(QFrame.NoFrame)
        self.setStyleSheet(self.STYLESHEET)

        # Display image as Pixmap in a QGraphicsScene
        self.scene = ImageViewScene()
        self.setScene(self.scene)

        # Store a local handle to the scene's current image pixmap
        self._pixmapHandle = None

        self.aspectRatioMode = QtCore.Qt.AspectRatioMode.KeepAspectRatio

        self.setHorizontalScrollBarPolicy(
            QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(
            QtCore.Qt.ScrollBarPolicy.ScrollBarAlwaysOff)

        # Stack of QRectF zoom boxes in scene coordinates.
        self.zoomStack = []

        # Flags for enabling/disabling mouse interaction.
        self.canZoom = True
        self.canPan = True

        # Accept drag and drop
        self.setAcceptDrops(True)

    def hasImage(self):
        """ Returns whether or not the scene contains an image pixmap.
        """
        return self._pixmapHandle is not None

    def clearImage(self):
        """ Removes the current image pixmap from the scene if it exists.
        """
        if self.hasImage():
            self.scene.removeItem(self._pixmapHandle)
            self._pixmapHandle = None

    def pixmap(self):
        """ Returns the scene's current image pixmap as a QPixmap, or else None if no image exists.
        :rtype: QPixmap | None
        """
        if self.hasImage():
            return self._pixmapHandle.pixmap()
        return None

    def image(self):
        """ Returns the scene's current image pixmap as a QImage, or else None if no image exists.
        :rtype: QImage | None
        """
        if self.hasImage():
            return self._pixmapHandle.pixmap().toImage()
        return None

    def setImage(self, file):
        image = QtGui.QImage(file)

        if image.isNull():
            QMessageBox.critical(None, "File open error",
                                 f"{file} is not image file!\n"
                                 "Allowed image types (.jpg, .jpeg, .png, .bmp).",
                                 QMessageBox.StandardButton.Close)
            return

        self.mainwindow.updateBgImage.emit(image)

        if isinstance(image, QtGui.QPixmap):
            pixmap = image
        elif isinstance(image, QtGui.QImage):
            pixmap = QtGui.QPixmap.fromImage(image)
        else:
            raise RuntimeError(
                "ImageView.setImage: Argument 'image' must be a QImage or QPixmap")

        pixmap = self.roundPixmapCorners(pixmap, 16)

        if self.hasImage():
            self._pixmapHandle.setPixmap(pixmap)
        else:
            self._pixmapHandle = self.scene.addPixmap(pixmap)

        self.setSceneRect(QtCore.QRectF(pixmap.rect()))
        self.updateView()

    def loadImageFromFile(self, filename=None):
        """ Load an image from file.
        Without any arguments, loadImageFromFile() will popup a file dialog to choose the image file.
        With a fileName argument, loadImageFromFile(fileName) will attempt to load the specified image file directly.
        """

        log.imgview.debug("loading image from file...")

        if not filename:
            filename, _ = QFileDialog.getOpenFileName(
                self, "Open image file.", filter='Images (*.png *.jpeg *.jpg *.bmp)')

        if filename and os.path.isfile(filename):
            image = QtGui.QImage(filename)
            self.setImage(image)

    def updateView(self):
        """ Show current zoom (if showing entire image, apply current aspect ratio mode).
        """

        if not self.hasImage():
            return

        self.fitInView(self.sceneRect(), self.aspectRatioMode)

    def roundPixmapCorners(self, pixmap: QtGui.QPixmap, radius: int) -> QtGui.QPixmap:
        rounded = QtGui.QPixmap(pixmap.size())
        rounded.fill(QtGui.QColor("transparent"))

        painter = QtGui.QPainter(rounded)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.setBrush(QtGui.QBrush(pixmap))
        painter.setPen(QtCore.Qt.NoPen)
        painter.drawRoundedRect(pixmap.rect(), radius, radius)
        painter.end()

        return rounded

    def wheelEvent(self, event):
        zoomInFactor = 1.5
        zoomOutFactor = 1 / zoomInFactor

        # Set Anchors
        self.setTransformationAnchor(QGraphicsView.NoAnchor)
        self.setResizeAnchor(QGraphicsView.NoAnchor)

        # Save the scene pos
        oldPos = self.mapToScene(event.position().toPoint())

        # Zoom
        if event.angleDelta().y() > 0:
            zoomFactor = zoomInFactor
        else:
            zoomFactor = zoomOutFactor
        self.scale(zoomFactor, zoomFactor)

        # Get the new position
        newPos = self.mapToScene(event.position().toPoint())

        # Move scene to old position
        delta = newPos - oldPos
        self.translate(delta.x(), delta.y())

    def mousePressEvent(self, event):
        """ Start mouse pan or zoom mode.
        """
        scenePos = self.mapToScene(event.position().toPoint())
        if event.button() == QtCore.Qt.LeftButton:
            if self.canPan:
                self.setDragMode(QGraphicsView.ScrollHandDrag)
            self.leftMouseButtonPressed.emit(scenePos.x(), scenePos.y())
        QGraphicsView.mousePressEvent(self, event)

    def mouseReleaseEvent(self, event):
        """ Stop mouse pan or zoom mode (apply zoom if valid).
        """
        QGraphicsView.mouseReleaseEvent(self, event)
        scenePos = self.mapToScene(event.position().toPoint())
        if event.button() == QtCore.Qt.LeftButton:
            self.setDragMode(QGraphicsView.NoDrag)
            self.leftMouseButtonReleased.emit(scenePos.x(), scenePos.y())

    def mouseDoubleClickEvent(self, event):
        """ Show entire image.
        """
        scenePos = self.mapToScene(event.position().toPoint())
        if event.button() == QtCore.Qt.LeftButton:
            self.leftMouseButtonDoubleClicked.emit(scenePos.x(), scenePos.y())
        elif event.button() == QtCore.Qt.RightButton:
            if self.canZoom:
                self.zoomStack = []
                self.updateView()
            self.rightMouseButtonDoubleClicked.emit(scenePos.x(), scenePos.y())
        QGraphicsView.mouseDoubleClickEvent(self, event)
