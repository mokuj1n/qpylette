from PySide6 import QtCore
from PySide6.QtGui import QMovie
from PySide6.QtWidgets import QLabel, QVBoxLayout, QWidget


class Loader(QWidget):

    DEFAULT_SPEED = 120

    def __init__(self, filename) -> None:
        super().__init__()

        self.vbox = QVBoxLayout()
        self.vbox.setAlignment(QtCore.Qt.AlignmentFlag.AlignHCenter)

        self.movie = QMovie(filename)
        self.movie.setSpeed(self.DEFAULT_SPEED)
        self.movie_label = QLabel()
        self.movie_label.setAlignment(QtCore.Qt.AlignCenter)
        self.movie_label.setMovie(self.movie)

        self.vbox.addWidget(self.movie_label)

        self.setLayout(self.vbox)

    def movie_start(self):
        self.movie.start()
        self.setVisible(True)

    def movie_stop(self):
        self.movie.stop()
        self.setVisible(False)
