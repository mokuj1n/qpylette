from PySide6.QtCore import QEasingCurve, QPropertyAnimation
from PySide6.QtWidgets import QGraphicsOpacityEffect, QStackedLayout, QWidget


class CustomStackedWidget(QWidget):

    DEFAULT_ANIM_DURATION = 500

    def __init__(self, parent=None) -> None:
        super().__init__(parent=parent)

        effect = QGraphicsOpacityEffect(self)
        self.setGraphicsEffect(effect)
        self.opacity_animation = QPropertyAnimation(effect, b"opacity")
        self.opacity_animation.setEasingCurve(QEasingCurve.InOutCubic)
        self.opacity_animation.setStartValue(0)
        self.opacity_animation.setEndValue(1)
        self.opacity_animation.setDuration(self.DEFAULT_ANIM_DURATION)


class CustomStackedLayout(QStackedLayout):

    def __init__(self):
        super().__init__()

        self.currentChanged.connect(self.play_animation)

    def play_animation(self, index):
        widget = self.currentWidget()
        widget.opacity_animation.start()
