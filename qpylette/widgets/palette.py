from PySide6 import QtCore, QtGui, QtWidgets
from qpylette.utils import config


class PaletteButton(QtWidgets.QPushButton):

    def __init__(self, color):
        super().__init__()
        self.setMinimumSize(*config.PALETTEBUTTON_SIZE)
        self.setSizePolicy(
            QtWidgets.QSizePolicy.Policy.Expanding,
            QtWidgets.QSizePolicy.Policy.MinimumExpanding,
        )
        self.color = color

    def paintEvent(self, event):
        pen = QtGui.QPen()
        pen.setWidth(0)
        pen.setColor(QtGui.QColor(self.color))

        painter = QtGui.QPainter(self)
        painter.setPen(pen)
        painter.setBrush(QtGui.QColor(self.color))
        painter.setRenderHint(QtGui.QPainter.Antialiasing, True)
        rectf = QtCore.QRectF(self.rect())
        painter.drawRoundedRect(rectf, 5, 5)


class _PaletteLinearBase(QtWidgets.QWidget):
    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent=parent, *args, **kwargs)

        self.colors = []

        self.central_layout = QtWidgets.QVBoxLayout()

        self.scroll_area = PaletteHScrollArea()
        self.scroll_widget = QtWidgets.QWidget()
        self.hbox = QtWidgets.QHBoxLayout()
        self.scroll_widget.setLayout(self.hbox)

        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setWidget(self.scroll_widget)

        self.central_layout.addWidget(self.scroll_area)
        self.setLayout(self.central_layout)

    def updatePalette(self, colors):
        self.colors = colors
        self.clearPalette()

        for color in self.colors:
            cb = PaletteButton(color)
            self.hbox.addWidget(cb)

    def clearPalette(self):
        if self.hbox and not self.hbox.count():
            return

        for index in reversed(range(self.hbox.count())):
            self.hbox.itemAt(index).widget().setParent(None)


class HorizontalPalette(_PaletteLinearBase):
    layoutvh = QtWidgets.QHBoxLayout


class PaletteHScrollArea(QtWidgets.QScrollArea):

    STYLESHEET = """
    background-color: rgba(0, 0, 0, 0);
    border: none;
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setStyleSheet(self.STYLESHEET)

        self.setAlignment(
            QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.verticalScrollBar().setDisabled(True)
        self.horizontalScrollBar().setSingleStep(10)

    def wheelEvent(self, event: QtGui.QWheelEvent):
        delta = event.angleDelta().y()
        x = self.horizontalScrollBar().value()
        self.horizontalScrollBar().setValue(x - delta)
