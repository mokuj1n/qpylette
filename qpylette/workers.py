from PySide6 import QtCore
from PySide6.QtCore import Signal, Slot

from qpylette.utils import log


class ColorzWorker(QtCore.QRunnable):

    class WorkerSignals(QtCore.QObject):
        start = Signal(bool)
        finished = Signal(bool)
        error = Signal(str)
        result = Signal(list)

    def __init__(self, fn, *args, **kwargs):
        super().__init__()

        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = self.WorkerSignals()

    @Slot()
    def run(self):
        try:
            log.workers.debug('Trying run a ColorzWorker')
            self.signals.start.emit(True)
            result = self.fn(*self.args, **self.kwargs)
        except Exception as error:
            log.workers.error(f'ColorzWorkerError: {error}')
            self.signals.error.emit(error)
        else:
            self.signals.result.emit(result)
            log.workers.debug(f'ColorzWorker got result: {result}')
        finally:
            self.signals.finished.emit(True)
            log.workers.debug(f'ColorzWorker finished')
