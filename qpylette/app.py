import sys
from os import environ

from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QApplication

import qpylette
import qpylette.resources
from qpylette import mainwindow, objects
from qpylette.utils import log


def run(args, argv):
    """ Run application """

    if args.version:
        print(f'qpylette v{qpylette.__version__}')
        sys.exit(0)

    log.init.debug("Initializing application...")

    suppress_qt_warnings()

    app = Application(argv)
    objects.qapp = app

    init(args)

    return qt_mainloop(app)


def init(args):
    """ Initialize everything """

    log.init.debug("Starting init...")

    _init_icon()

    log.init.debug("Initializing MainWindow")
    window = mainwindow.MainWindow()
    objects.window = window
    objects.window.setWindowTitle('qpylette')


def _init_icon():
    """ Initialize icons """
    filename = ':resources/icons/qpylette.png'
    icon = QIcon(filename)

    if icon.isNull():
        log.init.warning(f'Failed to load icon: {filename}')
    else:
        objects.qapp.setWindowIcon(icon)


def qt_mainloop(app):
    """ Wrapper for Qt main loop """
    return app.exec()


def suppress_qt_warnings() -> None:
    log.init.debug("Setup Qt environment variables...")
    environ["QT_DEVICE_PIXEL_RATIO"] = "0"
    environ["QT_AUTO_SCREEN_SCALE_FACTOR"] = "1"
    environ["QT_SCREEN_SCALE_FACTORS"] = "1"
    environ["QT_SCALE_FACTOR"] = "1"


def exit(code):
    sys.exit(code)


class Application(QApplication):
    def __init__(self, argv) -> None:
        super().__init__(argv)
