
""" Various global objects """

from typing import cast

from PySide6.QtWidgets import QApplication, QMainWindow

qapp = cast('QApplication', None)
window = cast('QMainWindow', None)
