
import os

from PySide6.QtCore import QThreadPool, Signal, Slot
from PySide6.QtGui import QImage
from PySide6.QtWidgets import QMainWindow, QVBoxLayout, QWidget

from qpylette import colorz
from qpylette.utils import config, log
from qpylette.utils.helpers import get_colors
from qpylette.widgets import (CustomStackedLayout, CustomStackedWidget,
                              HorizontalPalette, ImageView, Loader)
from qpylette.workers import ColorzWorker


class MainWindow(QMainWindow):

    updateBgImage = Signal(QImage)

    def __init__(self) -> None:
        super().__init__()
        self.setMinimumSize(*config.MAINWINDOW_SIZE)
        self.resize(*config.MAINWINDOW_SIZE)

        # ThreadPool
        self.threadPool = QThreadPool()

        # MainWindow Layouts
        self.window_layout = QVBoxLayout()
        self.stack_layout = CustomStackedLayout()
        self.main_layout = QVBoxLayout()
        self.loader_layout = QVBoxLayout()

        self.window_layout.addLayout(self.stack_layout)

        # Central Widget
        self.central_widget = QWidget()
        self.central_widget.setLayout(self.window_layout)
        self.setCentralWidget(self.central_widget)
        self.centralWidget().layout().setContentsMargins(0, 0, 0, 0)

        # | Main Widget (Image View + Color Palette)
        self.main_widget = CustomStackedWidget()
        self.main_widget.setLayout(self.main_layout)

        # |--> Image View
        self.image_view = ImageView(self)
        self.main_layout.addWidget(self.image_view)
        self.main_layout.setStretchFactor(self.image_view, 7)

        # |--> Color Palette
        self.color_palette = HorizontalPalette(self)
        self.main_layout.addWidget(self.color_palette)
        self.main_layout.setStretchFactor(self.color_palette, 2)

        self.stack_layout.addWidget(self.main_widget)

        # | Loader Widget
        self.loader_widget = CustomStackedWidget()
        self.loader_widget.setLayout(self.loader_layout)

        # |--> Loader Widget Animation

        # |--> Loader
        self.loader = Loader(':/resources/gif/loader.gif')
        self.loader_layout.addWidget(self.loader)

        self.stack_layout.addWidget(self.loader_widget)

        # Widgets Connection
        self._connect()

        # Show Main Window
        self.stack_layout.setCurrentWidget(self.main_widget)
        self.show()

    def _connect(self):
        self.updateBgImage.connect(self.updateBackgroundImage)
        self.image_view.scene.drop_file_signal.connect(self.drop_file_signal)

    def open_image(self, file):
        self.image_view.setImage(file)

        worker = ColorzWorker(colorz.colorz, file, config.NUM_OF_COLORS)
        worker.signals.result.connect(self.show_palette)
        worker.signals.start.connect(self.show_loader)
        worker.signals.finished.connect(self.hide_loader)

        self.threadPool.start(worker)

    @Slot(bool)
    def hide_loader(self):
        self.stack_layout.setCurrentWidget(self.main_widget)
        self.loader.movie_stop()

    @Slot(bool)
    def show_loader(self):
        self.stack_layout.setCurrentWidget(self.loader_widget)
        self.loader.movie_start()

    @Slot(list)
    def show_palette(self, raw_colors):
        colors = get_colors(raw_colors)
        self.color_palette.updatePalette(colors)

    @Slot(str)
    def drop_file_signal(self, file):
        log.imgview.debug(f"Droped file: {file}")
        self.open_image(file)

    @Slot(QImage)
    def updateBackgroundImage(self, image):
        log.mainwindow.debug("Update background image...")
