import os.path

__author__ = "Vadim Trifonov"
__copyright__ = "Copyright 2020-2022 Vadim Trifonov"
__license__ = "MIT"
__maintainer__ = __author__
__email__ = "retroarefobia@gmail.com"
__version__ = "0.3.0"
__version_info__ = tuple(int(part) for part in __version__.split('.'))
__description__ = "A simple program that showing the dominant colors of the image."

basedir = os.path.dirname(os.path.realpath(__file__))
