import argparse
import sys


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', default=False, required=False,
                        action='store_true', help="Show program version")

    parser.add_argument('image', nargs='?', default=None, help="image path")

    return parser


def main():
    parser = get_parser()
    argv = sys.argv[1:]
    args = parser.parse_args(argv)

    from qpylette import app

    return app.run(args, argv)
