
# *qpylette*

The *qpylette* this is a simple Qt application that shows the dominant colors in the image. The application uses the ***k-means*** algorithm to search for dominant colors.

<table align="center">
<tr>
    <td align="center" width="9999" height="500">
        <img src="/doc/img/main.png" align="center" alt="main.png">
    </td>
</tr>
</table>

## Run app
```
python -m qpylette
```


